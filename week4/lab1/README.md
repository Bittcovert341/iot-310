# Multiple Containers & Serverless [Continued]

For this week, please review Week 4 video for any in-class recap. 

We are going to finish our own implementation of our Sensor Gateway. 

We will detail what's happening in a Serverless app.


## Walkthrough

### Install Docker Compose

Ensure `pip` (version 6.0) is installed:

```bash
# https://pip.pypa.io/en/stable/installing/
pi$ wget https://bootstrap.pypa.io/get-pip.py

pi$ sudo python get-pip.py
```

Exit your session and SSH back into your RPi.

Install `docker-compose` using `pip`.

```bash
# https://docs.docker.com/compose/install/
# Expects pip version greater than 6.0
pi$ sudo pip install docker-compose
```

### Verify `docker-compose` install

```bash
pi$ docker-compose version
# docker-compose version 1.14.0, build c7bdf9e
```

## Create a `docker-compose.yml` file
[https://docs.docker.com/compose/gettingstarted/#step-3-define-services-in-a-compose-file](https://docs.docker.com/compose/gettingstarted/#step-3-define-services-in-a-compose-file)


