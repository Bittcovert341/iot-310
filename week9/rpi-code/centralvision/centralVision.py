# From: https://docs.microsoft.com/en-us/azure/cognitive-services/computer-vision/quickstarts/python#analyze-an-image-with-computer-vision-api-using-python-a-nameanalyzeimage-a

########### Python 2.7 #############
import httplib, urllib, base64, json
import os, sys, socket, time
import redis
import paho.mqtt.client as paho

#### mqtt connection info
hostname = "iot0bcc"
brokerHost = "iot.eclipse.org"     # Local MQTT broker
brokerPort   = 1883          # Local MQTT port
brokerTopic = "Judge-Bike-Status"  # Local MQTT topic to monitor
brokerTimeout = 120          # Local MQTT session timeout


# The callback for when a CONNACK message is received from the broker.
def on_connect(client, userdata, flags, rc):
    print "CONNACK received with code %d." % rc

mqttc = paho.Client()
mqttc.on_connect = on_connect
mqttc.connect(brokerHost, brokerPort, brokerTimeout)


###############################################
#### Update or verify the following values. ###
###############################################

# Replace the subscription_key string value with your valid subscription key.
subscription_key = os.getenv("AZURE_COMPUTER_VISION_KEY")

# Replace or verify the region.
#
# You must use the same region in your REST API call as you used to obtain your subscription keys.
# For example, if you obtained your subscription keys from the westus region, replace 
# "westcentralus" in the URI below with "westus".
#
# NOTE: Free trial subscription keys are generated in the westcentralus region, so if you are using
# a free trial subscription key, you should not need to change this region.
# uri_base = 'westcentralus.api.cognitive.microsoft.com'
uri_base = os.getenv("AZURE_COMPUTER_VISION_REGION_URI")

headers = {
    # Request headers.
    'Content-Type': 'application/json',
    'Ocp-Apim-Subscription-Key': subscription_key,
}

params = urllib.urlencode({
    # Request parameters. All of them are optional.
    'visualFeatures': 'Categories,Description,Color',
    'language': 'en',
})

print "Establishing redis connection."
r = redis.StrictRedis(host='queue', port=6379, db=0)
print "Established redis connection."

while True:
    # The URL of a JPEG image to analyze.
    url = r.lpop("imageprocessing")
    print "url: "
    print url
    
    if url:
        print "Found url: {}".format(url)
        mqttc.publish(brokerTopic, {'image_url': url})

    time.sleep(1)

    ####################################
