from picamera import PiCamera
from time import sleep

camera = PiCamera()

camera.start_preview()
sleep(20)
camera.stop_preview()

camera.rotation = 180
camera.start_preview()

camera.start_preview()

#wait for light to adjust
sleep(3)

#Set a timestamp
timestamp = int(time.time())

#Add a filename with a timestamp
filename = "img_{}.jpg".format(str(timestamp))
camera.capture(filename)

camera.stop_preview()

print 'File has been stored as: {}'.format(filename)
